package default;

/**
 * This class provides basic mathematical operations and greetings.
 */
public class MyClass {

    /**
     * Adds two integers.
     *
     * @param a The first integer.
     * @param b The second integer.
     * @return The sum of a and b.
     */
    public int add(int a, int b){
        return a + b;
    }

    /**
     * Concatenates two strings.
     *
     * @param a The first string.
     * @param b The second string.
     * @return The concatenation of a and b.
     */
    public String add(String a, String b){
        return a + b;
    }

    /**
     * Subtracts one integer from another.
     *
     * @param a The integer to subtract from.
     * @param b The integer to subtract.
     * @return The result of subtracting b from a.
     */
    public int sub(int a, int b){
        return a - b;
    }

    /**
     * Multiplies two integers.
     *
     * @param a The first integer.
     * @param b The second integer.
     * @return The product of a and b.
     */
    public int mult(int a, int b){
        return a * b;
    }

    /**
     * Divides one integer by another.
     *
     * @param a The dividend.
     * @param b The divisor (cannot be zero).
     * @return The integer quotient of dividing a by b.
     */
    public int div(int a, int b){
        return a / b;
    }

    /**
     * Calculates the remainder of dividing one integer by another.
     *
     * @param a The dividend.
     * @param b The divisor (cannot be zero).
     * @return The remainder of dividing a by b.
     */
    public int mod(int a, int b){
        return a % b;
    }

    /**
     * Generates a greeting message.
     *
     * @param name The name to greet.
     * @return A greeting message containing the given name.
     */
    public String sayHello(String name){
        return "Hello " + name;
    }

}
